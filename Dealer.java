import java.util.ArrayList;
public class Dealer {

	/*This is the dealer class I created
	 * It's very similar to the Player class file
	 * I didn't feel that there was a need to include as many methods or constructors
	 * As the dealer doesn't have a name in my game
	 * Nor starting/ending balances
	 */
	ArrayList <Card> hand=new ArrayList<Card>(0);
	public int handsum=0;
	
	public void addCard(Card n) //add to dealer's hand, identical to the addCard method in my Player class
	{
		System.out.println(n.toString());
		hand.add(n);
		// this.handsum = this.handsum + n.getValue();
		if (n.getValue() == 11 && this.handsum > 10) {
			this.handsum = this.handsum + 1;
		}
		if (n.getValue() == 11 && this.handsum <= 10) {
			this.handsum = this.handsum + n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum = this.handsum + n.getValue();
		}
		
		
	}
	
	public void isBlackJack()
	{
		this.handsum=21;
	}
	public int cardValue(Card n)
	{
		return n.getValue();
	}
	public int gethandsum() //returns the sum of the face card values in the dealer's hand
	{
		return handsum;
	}
	
	public int setScore()//this method needs to be called whenever a new round starts
	{
		return handsum=0;
	}
	
	
	
	
}
