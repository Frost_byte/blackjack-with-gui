// This class represents one blackjack player (or the dealer)

public class Player {
	// define fields here

	// This constructor creates a player.
	// If isDealer is true, this Player object represents the dealer.

	/*
	 * public Player(String playerName, boolean isDealer) { // complete this
	 * method
	 * 
	 * }
	 */

	public int cash;

	Hand hand = new Hand();

	public  int bet;
	public String name;
	public boolean dealer;
	public int dealerScore = 0;
	public Deck deck=new Deck();
	public Card d=deck.deal();
	
	

	BlackjackWindow screen;

	public Player(String name, int cash, boolean isDealer) {
		this.name = name;
		this.cash = cash;
		dealer = isDealer;
	}

	public void bet(int a) {
		this.bet = a;
	}

	public  int getBalance() {
		return this.cash;
	}

	public  double win() {
		this.cash = this.cash + this.bet;
		return this.cash;
		// if the player wins or ties, this method is called, and the player's
		// balance is increased
		// this method is only called if the player did not split
	}

	public double lose() {
		this.cash = this.cash - this.bet;
		return this.cash;
		// if the player loses against the dealer, this method is called, and
		// the player's balance is decreased
		// this method is only called if the player did not split
	}

	public void addCard(Card n) // adds a card to the player's first hand
	{
		System.out.println(n.toString());
		hand.addCard(n);

	}

	public String getName() {
		return this.name;
	}

	public int gethandsum() {
		return hand.gethandsum();
	}

	public void setScore() {
		this.hand.setScore();
	}

	public int getBet() {
		return this.bet;
	}

	// This method retrieves the player's hand of cards.
	public Hand getHand() {
		return this.hand; // replace this line with your code
	}
	
	public void removeHand()
	{
		this.hand.removeHand();
	}
	
	

	// This method deals two cards to the player (one face down if this is the
	// dealer).
	// The window input should be used to redraw the window whenever a card is
	// dealt.
	public void startRound(Deck deck, BlackjackWindow window) {

		// complete this method
		if (dealer == true) {
			GIO.displayMessage(this.name + ", you will now be dealt 1 card.");

			Card c = deck.deal();

			this.hand.addCard(c);

			window.redraw();

			//Card d = deck.deal();
			d.turnFaceDown();
			this.hand.addCard(d);

		} else {
			for (int i = 0; i < 1; i++) {
				Card c = deck.deal();

				this.hand.addCard(c);

				window.redraw();

			}

			GIO.displayMessage(this.name + ", the total of the cards you have so far is " + this.hand.gethandsum());
		}

	}

	// This method executes gameplay for one player.
	// If this player is the dealer:
	// - hits until score is at least 17
	// If this is an ordinary player:
	// - repeatedly asks the user if they want to hit (draw another card)
	// until either the player wants to stand (not take any more cards) or
	// his/her score exceeds 21 (busts).
	// The window input should be used to redraw the window whenever a card is
	// dealt or turned over.
	public void playRound(Deck deck, BlackjackWindow window) {
		// complete this method
		if (this.dealer == true) {
			//GIO.displayMessage("Dealer score is " + this.hand.gethandsum());
			d.turnFaceUp();
			while (hand.gethandsum() < 17) {
				Card a = deck.deal();
				this.hand.addCard(a);
				window.redraw();
				if (this.hand.gethandsum() > 21) {
					GIO.displayMessage("The dealer busted!");
					{
						break;
					}
				}

			}
		} else {
			String answer = GIO.readString(this.name + ", do you wish to hit or stand? Type hit for hit, stand for stand.");
		    
			
			while (answer.contentEquals("hit")==false&&answer.contentEquals("stand")==false)
			{
				GIO.displayMessage("Enter your input again.");
				answer=GIO.readString("What do you want to do?");
			}

			if (answer.contentEquals("hit")) {
				Card a = deck.deal();
				
				this.hand.addCard(a);
			
				window.redraw();
				if (hand.gethandsum() > 21) {
					GIO.displayMessage("You busted!");
					return;
				}
				String answer2 = GIO.readString("Do you want to keep on hitting? Hit yes for yes, no for no.");
				while (answer2.contentEquals("no")==false&&answer2.contentEquals("yes")==false)
				{
					GIO.displayMessage("Wrong answer.");
					answer2=GIO.readString("Yes or no?");
				}
				if (answer2.contentEquals("no")) {
					return;
				}

				while (GIO.readString("Continug hitting? ").contentEquals("yes")) {
					Card b = deck.deal();
					this.hand.addCard(b);
					window.redraw();
					if (hand.gethandsum() > 21) {
						GIO.displayMessage("Sorry, you busted!");
						{
							{break;}
						}
					}

					if (deck.isEmpty() == true) {
						GIO.displayMessage("We ran out of cards!");
						{
							{break;}
						}
					}

				}

			} else if (answer.contentEquals("stand")) {
				return;
			}
		}

	}

	// This method informs the player about whether they won, lost, or pushed.
	// It also discards the player's cards to prepare for the next round.
	// The window input should be used to redraw the window after cards are
	// discarded.
	public void finishRound(int dealerScore, BlackjackWindow window) {
		// complete this method
		
	}
}
