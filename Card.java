// This class represents one playing card.
public class Card
{
	// Card suits (provided for your convenience - use is optional)
	public static final int SPADES   = 0;
	public static final int HEARTS   = 1;
	public static final int CLUBS    = 2;
	public static final int DIAMONDS = 3;

	// Card faces (provided for your convenience - use is optional)
	public static final int ACE      = 1;
	public static final int TWO      = 2;
	public static final int THREE    = 3;
	public static final int FOUR     = 4;
	public static final int FIVE     = 5;
	public static final int SIX      = 6;
	public static final int SEVEN    = 7;
	public static final int EIGHT    = 8;
	public static final int NINE     = 9;
	public static final int TEN      = 10;
	public static final int JACK     = 11;
	public static final int QUEEN    = 12;
	public static final int KING     = 13;


	// define fields here
		int suit;
		int face;
		
		public boolean faceup=true;
		
		
		
		// This constructor builds a card with the given suit and face, turned face down.
		public Card(int cardSuit, int cardFace)
		{
			//fill in this method
			suit=cardSuit;
			face=cardFace;
			
		}

		// This method retrieves the suit (spades, hearts, etc.) of this card.
		public int getSuit()
		{
			//fill in this method
			return suit; //returns the int value of suit
		}
		
		// This method retrieves the face (ace through king) of this card.
		public int getFace()
		{
			//fill in this method
			return face; //returns the int value of face
		}
		
		// This method retrieves the numerical value of this card
		// (usually same as card face, except 1 for ace and 10 for jack/queen/king)
		public int getValue()
		{
			//fill in this method
			if (face>=2&&face<=10)
			{
				return face;
			}
			
			if (face==11||face==12||face==13)
			{
				return 10;
			}
			
			
			
			return 11; //ace value is either 1 or 11, depends on the total score player has
			
			
		}
		
		
		/*This toString method
		 * will create the proper
		 * syntax for the card that is printed to the console
		 * For example it will print 8 of clubs if the face value is 8 and the suit value is 2
		 */
		public String toString(){
			String r="";
			
			switch (face)
			{
			
			case Card.ACE:
			r+="Ace of ";
			break;
			
			case Card.TWO:
			r+="Two of ";	
			break;
			
			case Card.THREE:
			r+="Three of ";	
			break;
			
			case Card.FOUR:
			r+="Four of ";
		    break;
		    
			case Card.FIVE:
			r+="Five of ";	
			break;
			
			case Card.SIX:
			r+="Six of ";	
			break;
			
			case Card.SEVEN:
			r+="Seven of ";	
			break;
			
			case Card.EIGHT:
			r+="Eight of ";	
			break;
			
			case Card.NINE:
			r+="Nine of ";	
			break;
			
			case Card.TEN:
			r+="Ten of ";	
			break;
			
			case Card.JACK:
			r+="Jack of ";	
			break;
			
			case Card.QUEEN:
			r+="Queen of ";	
			break;
			
			case Card.KING:
			r+="King of ";	
			break;
				
		    
		    
			}
			switch (suit)
			{
			
			case Card.SPADES:
			r+="Spades ";
			break;
			
			case Card.HEARTS:
			r+="Hearts ";	
			break;
			
			case Card.CLUBS:
			r+="Clubs ";	
			break;
			
			case Card.DIAMONDS:
			r+="Diamonds ";	
			break;
			
			}
			
			return r;
			
			}
		
		

	// This method determines whether the front of the card should be visible.
	public boolean isFaceUp()
	{
		return faceup; // replace this line with your code
	}

	// This method records that the front of the card should be visible.
	public void turnFaceUp()
	{
		// complete this method
		faceup=true;
	}
	
	// This method records that only the back of the card should be visible.
	public void turnFaceDown()
	{
		// complete this method
		faceup=false;
	}
}

