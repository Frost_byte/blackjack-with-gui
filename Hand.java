import java.util.ArrayList;

// This class represents the set of cards held by one player (or the dealer).
public class Hand
{
	// define fields here
	ArrayList<Card> hand;
	public int handsum=0;
	
	// This constructor builds a hand (with no cards, initially).
	public Hand()
	{
		 hand = new ArrayList<Card>(0);
	}
	
	public void removeHand()
	{
		hand.clear();
	}
	
	
	public void blackjacktest()
	{ //used for testing purposes
		this.handsum=21;
	}
	
	
	
	public int cardValue(Card n) {
		return n.getValue();
		// this method returns the face value of the card
		// essential when determining if there is going to be insurance, splitting, etc.
	}
	// This method retrieves the size of this hand.
	public int getNumberOfCards()
	{
		return hand.size();
	}
	
	

	// This method retrieves a particular card in this hand.  The card number is zero-based.
	public Card getCard(int index)
	{
		return hand.get(index); // replace this line with your code
	}

	// This method takes a card and places it into this hand.
	public void addCard(Card n)
	{
		// complete this method
		System.out.println(n.toString());
		hand.add(n);
		// this.handsum = this.handsum + n.getValue();
		if (n.getValue() == 11 && this.handsum > 10) {
			this.handsum = this.handsum + 1;
		}
		if (n.getValue() == 11 && this.handsum <= 10) {
			this.handsum = this.handsum + n.getValue();
		}
		if (n.getValue() != 11) {
			this.handsum = this.handsum + n.getValue();
		}
	}
	
	

	// This method computes the score of this hand.
	public int gethandsum()
	{
		return this.handsum;
	}
	
	
	public ArrayList<Card> getfirsthand()
	{
		return this.hand;
	}
	
	public void setScore()
	{
		this.handsum=0;
	}
	
	
	
}
