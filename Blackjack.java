// This is the main program for the blackjack game.
public class Blackjack
{
	// This method should:
	//	- Ask the user how many people want to play (up to 3, not including the dealer).
	//	- Create an array of players.
	//	- Create a Blackjack window.
	// 	- Play rounds until the players want to quit the game.
	//	- Close the window.
	public static void main(String[] args)
	{
		// complete this method
       // Deck deck=new Deck();
		
		
	//	Dealer dealer=new Dealer();
		
		
		int players=GIO.readInt("How many players do you want to have? Enter up to 3");
		while (players>3||players<=0)
		{
			GIO.displayMessage("Wrong amount!");
	        players=GIO.readInt("How many players do you want to have?");
		}		
		
		
		Player[] players1 = new Player[players+1];
		
		
		for (int i=0;i<players;i++)
		{
          
			String name=GIO.readString("What's the name of player " +(i+1));
            int cash=100;
			Player c=new Player(name, cash,false);
			players1[i] = c;
            
		}
		players1[players1.length-1]=new Player ("Dealer",0,true); //last player is always dealer
		
		
		BlackjackWindow screen = new BlackjackWindow(players1);

		
		do	
		{
			screen.redraw();
			for (int i=0;i<players1.length;i++)
			{
				players1[i].setScore();
				players1[i].removeHand();

			}
			GIO.displayMessage("How much money does each player want to bet?");
			GIO.displayMessage("Enter in order, as first input is for player 1, etc.");
			for (int i=0;i<players;i++)
			{
				if (players1[i].getBalance()>0)
				{
				int a=GIO.readInt(players1[i].getName() + ", how much money do you want to bet?");
				while (a<=0||a>players1[i].getBalance())
				{
					GIO.displayMessage("Wrong amount, bet again.");
					a=GIO.readInt("How much are you going to bet?");
				}
				players1[i].bet(a);
				GIO.displayMessage("Your current balance as of this round is " + players1[i].getBalance());
				}
				
				if (players1[i].getBalance()<=0)
				{
					GIO.displayMessage(players1[i].getName() + ", you're not allowed to play.");
				}
			}
			
			
			playRound(players1, screen); 
		
		GIO.displayMessage("Do you guys want to start another blackjack round?");
		GIO.displayMessage("Decide on an answer as a group, and type yes for yes, no for no.");
		
		}
		while (GIO.readString("So? Do you guys want to continue again?").contentEquals("yes"));
		
	    screen.close();
	}

	// This method executes an single round of play (for all players).  It should:
	//	- Create and shuffle a deck of cards.
	//	- Start the round (deal cards) for each player, then the dealer.
	//	- Allow each player to play, then the dealer.
	//	- Finish the round (announce results) for each player.
	public static void playRound(Player[] players, BlackjackWindow window)
	{
		
		Deck deck=new Deck();
		deck.shuffle();
		
		for (int i=0;i<players.length;i++) //starting round
		{
			if (i==players.length-1)
			{
			window.redraw();
			players[i].startRound(deck, window);
			
			}
			else if (players[i].getBalance()>0)
			{
				window.redraw();
              players[i].startRound(deck, window);
              window.redraw();
              players[i].startRound(deck, window);
              window.redraw();
			}
		}
		
		
		for (int i=0;i<players.length;i++)
		{
			if (players[i].getBalance()>0&&i!=players.length-1)
			{
			window.redraw();
			players[i].playRound(deck, window);
			window.redraw();
			}
			
			else if (players[i].getBalance()<=0&&i!=players.length-1)
			{
				GIO.displayMessage(players[i].getName() + ", you are not allowed to play!");
			}
			
			else if (i==players.length-1)
			{
				window.redraw();
                players[i].playRound(deck, window);
                window.redraw();
			}
		}
		int length=players.length;
		int dealerScore=players[length-1].gethandsum();
		for (int i=0;i<length-1;i++)
		{
		GIO.displayMessage("Now to determine if " + players[i].getName() + " won, tied, or lost.");
		if (players[i].gethandsum() <= 21 && players[i].gethandsum() > dealerScore && dealerScore <= 21 && players[i].getBalance()>0) {
			GIO.displayMessage(players[i].getName() + ", you won!");
			GIO.displayMessage("Your balance is now " + players[i].win());

		}

		if (players[i].getBalance()>0&&players[i].gethandsum() <= 21 && players[i].gethandsum() == dealerScore && dealerScore <= 21) {
			GIO.displayMessage(players[i].getName() + ", you tied with the dealer.");
			GIO.displayMessage("Your balance is now " + players[i].getBalance());

		}

		if (players[i].getBalance()>0&&players[i].gethandsum() <= 21 && players[i].gethandsum() < dealerScore && dealerScore <= 21) {
			GIO.displayMessage("Sorry " + players[i].getName() + ", you did not win.");
			GIO.displayMessage("Balance is " + players[i].lose());
		}

		if (players[i].getBalance()>0&&players[i].gethandsum() > 21) {
			GIO.displayMessage("Sorry " + players[i].getName() + ", you did not win.");
			GIO.displayMessage("Balance is " + players[i].lose());
		}

		if (players[i].getBalance()>0&&dealerScore > 21 && players[i].gethandsum() <= 21) {
			GIO.displayMessage(players[i].getName() + ", you won!");
			GIO.displayMessage("Your balance is now " + players[i].win());

		}

		if (players[i].getBalance() <= 0) {
			GIO.displayMessage(players[i].getName() + ", you shouldn't even be playing anymore.");
		}

		}
		
	}
}

