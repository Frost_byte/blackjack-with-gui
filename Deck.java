import java.util.Random;

// This class represents the deck of cards from which cards are dealt to players.
public class Deck
{
	public Card[] array;//array is a good choice to use here
	public Card[] reducedArray;
	public static final int NBR_CARDS=52;
	int z=0;
	int i=-1;
	private int totalCardCount=0;
	
	
	// This constructor builds a deck of 52 cards.
	public Deck()
	{
		//fill this method in
		array= new Card[52];
		//new Card(s,f);
		
		
		for (int cardFace=Card.ACE;cardFace<=Card.KING;cardFace++)
		{
		
			for (int cardSuit=Card.SPADES;cardSuit<=Card.DIAMONDS;cardSuit++)
			{
				array[z]=new Card(cardSuit, cardFace);
				z++;
			
			//This is a nested for loop that creates the deck of cards
			
			}
			
		}
		
		
		
	}

	// This method takes the top card off the deck and returns it.
	public Card deal()
	{
		//fill this method in
		i++;
		
		//System.out.println(array[i]);
		if (array[i].getFace()==2||array[i].getFace()==3||array[i].getFace()==7)
		{
			totalCardCount=totalCardCount+1;
		}
		
		if (array[i].getFace()==4||array[i].getFace()==5||array[i].getFace()==6)
		{
			totalCardCount=totalCardCount+2;
		}
		
		if (array[i].getFace()==8||array[i].getFace()==1)
		{
			totalCardCount=totalCardCount+0;
		}
		
		if (array[i].getFace()==9)
		{
			totalCardCount=totalCardCount-1;
		}
		
		if (array[i].getFace()==10||array[i].getFace()==11||array[i].getFace()==12||array[i].getFace()==13)
		{
			totalCardCount=totalCardCount-2;
		}
		return array[i];	
		
}
	
	public int getCardCount()
	{
		return totalCardCount; //returns the card count
		
	}
	
	public Card[] reducedDeck() //creates the reduced deck of cards with the dealt cards as null
	{
		int j=i+1;
		int k=0;
		this.reducedArray=new Card[this.array.length];
		while (j<this.reducedArray.length)
		{
			this.reducedArray[k]=array[j];
			j++;
			k++;
		}
		
		return this.reducedArray;
	}
	// this method returns true if there are no more cards to deal, false otherwise
	public boolean isEmpty()
	{
		//fill this method in
		
		if (i>=51)//the index at i has moved beyond 52 cards... which is impossible as there is a max. of 52 cards in the deck
		{
		return true;
		}
		
		return false;
	}
	
	//this method puts the deck int some random order
	public void shuffle(){
		
		//fill this method in
		
		
		
		for (int i=52;i>0;i--)
		{
		    int y=(int)(Math.random()*i);
			Card temp=array[i-1];
			array[i-1]=array[y];
			array[y]=temp;
			
			
		}
		
		
	}
	
	public String toString()//the toString method
	{
		String s="";
		for (int i=0;i<array.length;i++)
		{
			s=s+array[i].toString()+" , ";
		}
		return s;
	}
	// This method returns the number of cards left in the deck.
	public int getSize()
	{
		return 0; // replace this line with your code
	}
}

